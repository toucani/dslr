# DSLR


## Installing

Just clone this repository.
To run the program you need python3 and pip.

## Using

Describe the dataset:
```
./code/describe.py datasets/dataset_train.csv
```

Find out similar features:
```
./code/scatter_plot.py datasets/dataset_train.csv
```

See all the features:
```
./code/pair_plot.py datasets/dataset_train.csv
```

See the subject with normal distribution on grades:
```
./code/histogram.py datasets/dataset_train.csv
```

Train the sorting hat:
```
./code/logreg_train.py datasets/dataset_train.csv
```

Sort the students:
```
./code/logreg_predict.py datasets/dataset_train.csv results.json
```

Check how accurate this is:
```
./code/accuracy_check.py datasets/dataset_train.csv houses.csv
```


## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=DSLR from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to UNIT Factory, for inspiration to do our best.
* to all UNIT Factory students, who shared their knowledge and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
