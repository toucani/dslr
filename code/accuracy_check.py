#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    accuracy_check.py                                  :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/18 19:52:45 by dkovalch          #+#    #+#              #
#    Updated: 2018/06/19 21:09:25 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of DSLR project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys

import common


def count_accuracy(file1, file2):
    data1 = common.get_raw_data(file1)
    data2 = common.get_raw_data(file2)

    del data1[0]
    del data2[0]
    common.drop_column(data1, 0)
    while len(data1[0]) > 1:
        common.drop_column(data1, 1)

    common.drop_column(data2, 0)
    while len(data2[0]) > 1:
        common.drop_column(data2, 1)

    assert len(data1) == len(data2)
    nailed = 0
    for ct in range(len(data1)):
        nailed += 1 if data1[ct][0] == data2[ct][0] else 0

    print("Accuracy: {}%, {}/{}".format(nailed / len(data1) * 100, nailed, len(data1)))


if __name__ == "__main__":
    try:
        file1 = sys.argv[1] if len(sys.argv) > 1 else "datasets/dataset_train.csv"
        file2 = sys.argv[2] if len(sys.argv) > 2 else "houses.csv"
        count_accuracy(file1, file2)
    except IOError as err:
        print(err)
