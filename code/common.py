#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    common.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/05/21 20:38:42 by dkovalch          #+#    #+#              #
#    Updated: 2018/06/17 10:24:09 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of DSLR project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import csv
import math


# Because Python3 cannot produce valid round!
# Python3 round(1.5) = 2
# Python3 round(6.5) = 6
def proper_round(number):
    fr, int = math.modf(number)
    if fr >= 0.5 or (fr > -0.5 and fr < 0):
        return round(math.ceil(number))
    return round(math.floor(number))


def drop_column(data, column):
    for line in data:
        del line[column]


def remove_index(names, data):
    if names[0] == "Index":
        del names[0]
        drop_column(data, 0)


def isNumeric(value):
    try:
        if not value:
            value = "0.0"
        float(value)
        return True
    except ValueError:
        return False


def slice_data(raw_data, empty_filler):
    num_columns = [col if isNumeric(raw_data[1][col]) else -1 for col in range(len(raw_data[1]))]
    num_columns = list(filter(lambda x: x >= 0, num_columns))
    values = [[raw_data[row][num_columns[col]:num_columns[col] + 1] for col in range(len(num_columns))]
              for row in range(len(raw_data))]
    values = [[col[0] for col in row] for row in values]
    names = values.pop(0)
    for row in range(len(values)):
        for col in range(len(values[row])):
            try:
                values[row][col] = float(values[row][col])
            except ValueError:
                values[row][col] = empty_filler
    return names, values


def get_raw_data(filename):
    csv_data = csv.reader(open(filename.strip()), delimiter=',')
    csv_data = [[line[col].strip() for col in range(len(line))] for line in csv_data]
    return csv_data


def get_numeric_data(filename, empty_filler=None):
    names, values = slice_data(get_raw_data(filename), empty_filler)
    remove_index(names, values)
    return names, values


def get_numeric_values_by_house(filename, empty_filler=None):
    raw_data = get_raw_data(filename)
    names, data = get_numeric_data(filename, empty_filler)
    house_index = raw_data[0].index("Hogwarts House")
    house_data = {}
    for line in range(1, len(data)):
        key = raw_data[line + 1][house_index]
        house_data.setdefault(key, []).append(data[line])
    return house_data, names
