#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    describe.py                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/29 20:40:10 by dkovalch          #+#    #+#              #
#    Updated: 2018/06/19 20:37:39 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of DSLR project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import math
import sys

from common import get_numeric_data, proper_round


def get_count(data):
    count = [len(data) for x in data[0]]
    return count


def get_mean(data, count):
    mean = [0.0 for x in range(len(data[0]))]
    for line in data:
        mean = [mean[x] + line[x] if line[x] is not None else 0. for x in range(len(line))]
    for ct in range(len(mean)):
        mean[ct] = mean[ct] / count[ct]
    return mean


def get_std(data, count, mean):
    std = [0.0 for x in range(len(data[0]))]
    for line in data:
        std = [std[x] + ((line[x] if line[x] is not None else 0. - mean[x]) ** 2) for x in range(len(line))]
    for ct in range(len(std)):
        std[ct] = math.sqrt(std[ct] / (count[ct] - 1))
    return std


# Nearest percentile
def get_percent(data, percentile):
    index = len(data) * percentile - 1
    index = proper_round(index)
    percentile = [sorted(data, key=lambda line: line[x] if line[x] is not None else 0.)[index][x] for x in
                  range(len(data[0]))]
    percentile = [x for x in percentile if x is not None]
    return percentile


def get_min(data):
    result = [sorted(data, key=lambda line: line[x] if line[x] is not None else 0.)[0][x] for x in range(len(data[0]))]
    result = [x for x in result if x is not None]
    return result


def get_max(data):
    result = [sorted(data, key=lambda line: line[x] if line[x] is not None else 0., reverse=True)[0][x] for x in
              range(len(data[0]))]
    result = [x for x in result if x is not None]
    return result


def do_describe(data):
    data_count = get_count(data)
    data_mean = get_mean(data, data_count)

    result = [["Count"] + data_count]
    result = result + [["Mean"] + data_mean]
    result = result + [["Std"] + get_std(data, data_count, data_mean)]
    result = result + [["Min"] + get_min(data)]
    result = result + [["25%"] + get_percent(data, 0.25)]
    result = result + [["50%"] + get_percent(data, 0.5)]
    result = result + [["75%"] + get_percent(data, 0.75)]
    result = result + [["Max"] + get_max(data)]
    return result


def describe(filename):
    names, values = get_numeric_data(filename)
    result = [[""] + names] + do_describe(values)
    return result


def print_result(result):
    widths = [max(len(name), 8) for name in result[0]]
    for line in result:
        print(line[0].ljust(widths[0]), end=" ")
        for col in range(1, len(line)):
            if type(line[col]) is float:
                out = "{:.6f}".format(line[col])
            else:
                out = str(line[col])
            print(out[0:widths[col]].rjust(widths[col]), end=",   ")
        print()


if __name__ == "__main__":
    try:
        filename = sys.argv[1] if len(sys.argv) > 1 else "datasets/dataset_train.csv"
        print_result(describe(filename.strip()))
    except IOError as err:
        print(err)
