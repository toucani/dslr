#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    histogram.py                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/05/06 10:48:40 by dkovalch          #+#    #+#              #
#    Updated: 2018/06/19 20:37:53 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of DSLR project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys
from operator import add
from operator import sub

import matplotlib.pyplot as plt

from common import get_numeric_values_by_house
from describe import get_percent


# Float range generator
def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step


# Counting percentiles for every subject, in every house data
# Percentiles: 0, 0.25, 0.5, 0.75, 1
# Returns dictionary {house : {percentile : values}}
def count_percentiles(house_data):
    house_percentile = {}
    for key in house_data:
        percents = {}
        for x in frange(0.0, 1.1, 0.25):
            percents[x] = get_percent(house_data[key], x)
        house_percentile[key] = percents
    return house_percentile


def get_distribution_data(filename):
    house_data, names = get_numeric_values_by_house(filename)

    # Percentiles for every subject per house
    house_percentiles = count_percentiles(house_data)

    # Getting deltas between house-to-house grades
    # Hardcode!!!
    house_deltas = {"SR": [], "RG": [], "GH": [], "HS": []}
    try:
        house_deltas["SR"] = [list(
            map(sub, house_percentiles['Slytherin'][x], house_percentiles['Ravenclaw'][x])
        ) for x in house_percentiles["Slytherin"].keys()]
        house_deltas["RG"] = [list(
            map(sub, house_percentiles['Ravenclaw'][x], house_percentiles['Gryffindor'][x])
        ) for x in house_percentiles["Slytherin"].keys()]
        house_deltas["GH"] = [list(
            map(sub, house_percentiles['Gryffindor'][x], house_percentiles['Hufflepuff'][x])
        ) for x in house_percentiles["Slytherin"].keys()]
        house_deltas["HS"] = [list(
            map(sub, house_percentiles['Hufflepuff'][x], house_percentiles['Slytherin'][x])
        ) for x in house_percentiles["Slytherin"].keys()]
    except KeyError as err:
        print("Not fair, noone from ", end='')
        print(err, end='')
        print(" found!")
        exit()

    # Flattenning percentile deltas by calculating mean deltas
    for key in house_deltas:
        res = [0.0 for x in range(len(house_deltas[key][0]))]
        for percentile in house_deltas[key]:
            res = list(map(add, res, percentile))
        res = [item / len(house_deltas[key]) for item in res]
        house_deltas[key] = res

    # Getting mean delta per column
    mean_delta = []
    for key in house_deltas:
        mean_delta = [0.0 for x in range(len(house_deltas[key]))]
        mean_delta = list(map(add, mean_delta, house_deltas[key]))
    mean_delta = [abs(item / len(house_deltas)) for item in mean_delta]

    # Now subject with minimum delta(with minimum deviation between percentiles between houses)
    # is our homogenius subject
    homo_index = mean_delta.index(min(mean_delta))
    homo_data = []
    for key in house_data:
        homo_data.append([line[homo_index] for line in house_data[key] if line[homo_index] is not None])

    return names[homo_index], homo_data, list(house_data.keys())


def draw_histosgram(name, data, legend,
                    xlabel="Grade",
                    ylabel="Amount",
                    show_grid=True):
    plt.title(name)
    colors = ['', '', '', '']
    colors[legend.index("Slytherin")] = 'xkcd:grass green'
    colors[legend.index("Gryffindor")] = 'xkcd:scarlet'
    colors[legend.index("Ravenclaw")] = 'xkcd:electric blue'
    colors[legend.index("Hufflepuff")] = 'xkcd:golden rod'
    plt.hist(data, color=colors, bins=18, histtype='barstacked')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend(legend)
    plt.grid(show_grid)
    plt.show()


if __name__ == "__main__":
    try:
        filename = sys.argv[1] if len(sys.argv) > 1 else "datasets/dataset_train.csv"
        name, data, legend = get_distribution_data(filename.strip())
        draw_histosgram(name, data, legend)
    except IOError as ex:
        print(ex)
