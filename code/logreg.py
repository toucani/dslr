#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    logreg.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/16 16:32:01 by dkovalch          #+#    #+#              #
#    Updated: 2018/06/17 19:27:28 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of DSLR project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import json
import math


def save_to_file(weights, filename):
    with open(filename, "w") as file:
        file.write(json.dumps(weights, sort_keys=True, indent=4))


def read_weights(filename):
    with open(filename, "r") as file:
        return json.loads(file.read())


def sigmoid(x):
    return 1.0 / (1.0 + math.exp(-x))


def predict(values, weights):
    result = weights[0]
    for ct in range(len(values)):
        result += values[ct] * weights[ct + 1]
    return sigmoid(result)


def scale_data(data):
    for house in data:
        # Looking for min and max
        max_values = [0.0] * len(data[house][0])
        min_values = [0.0] * len(data[house][0])
        for line in data[house]:
            for ct in range(len(line)):
                max_values[ct] = max(max_values[ct], line[ct])
                min_values[ct] = min(min_values[ct], line[ct])
        # Rescaling
        for line in data[house]:
            for ct in range(len(line)):
                line[ct] = (line[ct] - min_values[ct]) / (max_values[ct] - min_values[ct])
