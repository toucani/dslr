#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    logreg_predict.py                                  :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/17 10:18:38 by dkovalch          #+#    #+#              #
#    Updated: 2018/06/19 20:43:31 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of DSLR project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#


import operator
import sys

import common
import logreg


def write_predictions(predictions):
    with open("houses.csv", "w") as file:
        file.write("Index,Hogwarts House\n")
        for line in predictions:
            file.write("{},{}\n".format(line[0], line[1]))


def get_predictions(dataset_file, weights_file):
    weights = logreg.read_weights(weights_file)
    names, values = common.get_numeric_data(dataset_file, 0.0)
    if "Hogwarts House" in names:
        common.drop_column(values, names.index("Hogwarts House"))
        del names[names.index("Hogwarts House")]
    vmap = {"dummy house": values}
    logreg.scale_data(vmap)
    values = vmap["dummy house"]
    result = []
    for ct in range(len(values)):
        predictions = {}
        for house in weights:
            predictions[house] = logreg.predict(values[ct], weights[house])
        house = max(predictions.items(), key=operator.itemgetter(1))[0]
        result.append([ct, house])

    return result


if __name__ == "__main__":
    try:
        dataset = sys.argv[1] if len(sys.argv) > 1 else "datasets/dataset_train.csv"
        weights = sys.argv[2] if len(sys.argv) > 2 else "results.json"
        write_predictions(get_predictions(dataset, weights))
    except IOError as err:
        print(err)
