#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    logreg_train.py                                    :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/15 21:04:38 by dkovalch          #+#    #+#              #
#    Updated: 2018/06/19 21:15:55 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of DSLR project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#


import concurrent.futures
import sys
from subprocess import call

import common
import logreg


def update_weights(data, house, learning_rate, weights):
    for curr_house, curr_data in data.items():
        result = 1 if curr_house == house else 0
        for line in curr_data:
            prediction = logreg.predict(line, weights)
            for ct in range(1, len(weights)):
                weights[ct] += learning_rate * (result - prediction) * prediction * (1 - prediction) * line[ct - 1]
            weights[0] += learning_rate * (result - prediction)
    return weights


def do_train(data, house, learning_rate, accuracy, iterations):
    weights = [0.0] * (len(data[house][0]) + 1)
    while iterations > 0:
        new_weights = update_weights(data, house, learning_rate, weights.copy())

        # Checking that at least one weight delta still has meaning(bigger than accuracy)
        for ct in range(len(weights)):
            if abs(weights[ct] - new_weights[ct]) < accuracy:
                # print("{}, exiting, weights\n{}".format(house, weights))
                return weights

        weights = new_weights
        iterations -= 1
    return weights


def train(filename, weights_filename):
    data, names = common.get_numeric_values_by_house(filename, 0.0)
    logreg.scale_data(data)
    weights = {}
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for house in data:
            weights[house] = executor.submit(do_train, data, house, 1e-1, 1e-7, 500)

    for house in data:
        weights[house] = weights[house].result()

    logreg.save_to_file(weights, weights_filename)


if __name__ == "__main__":
    try:
        filename = sys.argv[1] if len(sys.argv) > 1 else "datasets/dataset_train.csv"
        weights = sys.argv[2] if len(sys.argv) > 2 else "results.json"
        train(filename, weights)
        # Checking accuracy
        call(["./logreg_predict.py", "datasets/dataset_train.csv", weights])
        call(["./accuracy_check.py", "houses.csv", "datasets/dataset_train.csv"])
    except IOError as err:
        print(err)
    except KeyboardInterrupt:
        pass
