#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    pair_plot.py                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/12 18:55:32 by dkovalch          #+#    #+#              #
#    Updated: 2018/06/19 20:38:30 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of DSLR project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys

import matplotlib.pyplot as plt

from common import get_numeric_values_by_house


def plot_it(filename):
    house_data, names = get_numeric_values_by_house(filename)

    for feature1 in range(len(names)):
        for feature2 in range(len(names)):
            plt.subplot(len(names), len(names), feature1 * len(names) + feature2 + 1)
            data, xlabel, ylabel = get_data(house_data, names, feature2, feature1)
            ylabel = "" if feature2 is not 0 else ylabel[:6]
            xlabel = "" if feature1 is not len(names) - 1 else xlabel[:6]
            add_plot(data, ylabel, xlabel)
    plt.subplots_adjust(top=0.98, bottom=0.05, left=0.05, right=0.98,
                        hspace=0.36, wspace=0.34)
    plt.show()


def get_data(house_data, names, id1, id2):
    data = {}
    try:
        for house in ('Slytherin', 'Gryffindor', 'Ravenclaw', 'Hufflepuff'):
            data.setdefault(house, {}).setdefault('x', []).append(
                [line[id1] for line in house_data[house]])
            data.setdefault(house, {}).setdefault('y', []).append(
                [line[id2] for line in house_data[house]])
    except KeyError as err:
        print("Not fair, noone from ", end='')
        print(err, end='')
        print(" found!")
        exit()
    return data, names[id1], names[id2]


def add_plot(data, ylabel, xlabel):
    plt.scatter(data['Slytherin']['x'], data['Slytherin']['y'], c='xkcd:grass green', label='Slytherin')
    plt.scatter(data['Gryffindor']['x'], data['Gryffindor']['y'], c='xkcd:scarlet', label='Gryffindor')
    plt.scatter(data['Ravenclaw']['x'], data['Ravenclaw']['y'], c='xkcd:electric blue', label='Ravenclaw')
    plt.scatter(data['Hufflepuff']['x'], data['Hufflepuff']['y'], c='xkcd:golden rod', label='Hufflepuff')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if not ylabel:
        plt.yticks([], [])
    if not xlabel:
        plt.xticks([], [])
    plt.grid(False)


if __name__ == "__main__":
    try:
        filename = sys.argv[1] if len(sys.argv) > 1 else "datasets/dataset_train.csv"
        plot_it(filename)
    except IOError as err:
        print(err)
