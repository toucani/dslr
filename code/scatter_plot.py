#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    scatter_plot.py                                    :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/05/21 20:00:11 by dkovalch          #+#    #+#              #
#    Updated: 2018/06/19 20:38:36 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of DSLR project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys

import matplotlib.pyplot as plt

from common import get_numeric_values_by_house, proper_round


def combine_plots(filename, features):
    house_data, names = get_numeric_values_by_house(filename)

    # Settings screen size for the plots
    name, data, xlabel, ylabel = get_data(house_data, names,
            features[0], features[1])
    add_plot(name, data, xlabel, ylabel)
    plt.show()


def get_data(house_data, names, feature1, feature2):
    data = {}

    id1 = names.index(feature1)
    id2 = names.index(feature2)

    try:
        for house in ('Slytherin', 'Gryffindor', 'Ravenclaw', 'Hufflepuff'):
            data.setdefault(house, {}).setdefault('x', []).append(
                [line[id1] for line in house_data[house]])
            data.setdefault(house, {}).setdefault('y', []).append(
                [line[id2] for line in house_data[house]])
    except KeyError as err:
        print("Not fair, noone from ", end='')
        print(err, end='')
        print(" found!")
        exit()
    return feature1 + " - " + feature2, data, names[id1], names[id2]


def add_plot(name, data, ylabel, xlabel, show_grid=True):
    plt.title(name)
    plt.scatter(data['Slytherin']['x'], data['Slytherin']['y'], c='xkcd:grass green', label='Slytherin')
    plt.scatter(data['Gryffindor']['x'], data['Gryffindor']['y'], c='xkcd:scarlet', label='Gryffindor')
    plt.scatter(data['Ravenclaw']['x'], data['Ravenclaw']['y'], c='xkcd:electric blue', label='Ravenclaw')
    plt.scatter(data['Hufflepuff']['x'], data['Hufflepuff']['y'], c='xkcd:golden rod', label='Hufflepuff')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.grid(show_grid)


if __name__ == "__main__":
    try:
        filename = sys.argv[1] if len(sys.argv) > 1 else "datasets/dataset_train.csv"
        combine_plots(filename, ("Astronomy", "Defense Against the Dark Arts"))
    except IOError as err:
        print(err)
